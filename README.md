This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Before running the app it is required to install:
npm package manager                         --- npm install
redux and react-redux labrary               --- npm redux react-redux --save
bootstrap library                           --- npm bootstrapp --save

Then from the oot directory run the script  --- npm start
