import {combineReducers} from 'redux'
import clients from './clients'
import selectedClient from './selectedClient'
import foundClients from './foundClients'

export default combineReducers({
    clients,
    selectedClient,
    foundClients
})