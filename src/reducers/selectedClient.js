export default function selectedClient(state='', action){
    if(action.type === 'SELECT_CLIENT'){
        return action.selectedClient;
    }
    return state;
}