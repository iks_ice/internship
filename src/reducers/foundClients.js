export default function foundClients(state='', action){
    if(action.type === 'FIND_CLIENT'){
        return action.foundClients;
    }
    return state;
}