import React            from 'react'
import ReactDOM         from 'react-dom'
import App              from './components/App'
import {Provider}       from 'react-redux'
import {createStore}    from 'redux'
import combineReducers  from './reducers'
import                  'bootstrap/dist/css/bootstrap.min.css'
import                  './components/index.css'

const store = createStore(combineReducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
, document.getElementById('root'))