import React from 'react'
import {connect} from 'react-redux'

function ClientInfo(props){
    console.log(props)
    if(props.selectedClient === ''){
    return (
        <div className="pattern">
            <div className="row">
                <div className="col-4">
                    <img  alt="photo"/>
                </div>
                <div className="col-8">
                    <h4>First name Last name</h4>
                    <h6>Occupation and company</h6> 
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <p>Email:</p>
                    <p>Phone:</p>
                </div>
                <div className="col">
                    <p>Street:</p>
                    <p>City:</p>
                    <p>Zip code:</p>
                    <p>Country:</p>

                </div>
            </div>
        </div>
    )
}   else {
        const {avatar,firstName, lastName}   = props.selectedClient.general 
        const{company,title}                 = props.selectedClient.job
        const{email,phone}                   = props.selectedClient.contact
        const{street,city, zipCode,country}  = props.selectedClient.address  
        return (
            <div className="clientinfo">
                <div className="row">
                    <div className="col-4">
                        <img src={avatar}/>
                    </div>
                    <div className="col-8">
                        <h4>{`${firstName} ${lastName}`}</h4>
                        <h6>{`${title} ${company}`}</h6> 
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <p>Email:{` ${email}`}</p>
                        <p>Phone:{` ${phone}`}</p>
                    </div>
                    <div className="col">
                        <p>Street:{ `${street}`}</p>
                        <p>City:{` ${city}`}</p>
                        <p>Zip code:{` ${zipCode}`}</p>
                        <p>Country:{` ${country}`}</p>

                    </div>
                </div>
            </div>
        )
    }
}
export default connect(
    state => ({
        selectedClient: state.selectedClient
    })
)(ClientInfo)