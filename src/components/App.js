import React from 'react'
import {connect} from 'react-redux'
import ClientList from './ClientList'
import ClientInfo from './ClientInfo'

class App extends React.Component {
    onSearch = () => {
        let str = this.inputVal.value.toLowerCase();
        let foundClients = this.props.clients.filter(client =>{
            let clientStr = JSON.stringify(client).toLowerCase();
            return clientStr.indexOf(str) !== -1;
        });
        if(foundClients) {
            this.props.search(foundClients)
        }
    }
    render() {
        
        return(
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className='search'>
                            <input onInput={this.onSearch.bind(this)} ref={input=>{this.inputVal=input}}/>
                        </div>
                        <ClientList />
                    </div>
                    <div className="col">
                        <ClientInfo />
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        clients :state.clients
    }),
    dispatch => ({
        search: (foundClients) => {
            dispatch({type: 'FIND_CLIENT', foundClients: foundClients})
        }
    })
)(App)