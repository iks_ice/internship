import React from 'react'
import Client from './Client'
import {connect} from 'react-redux'

function ClientList(props){
    console.log(props)
    if(props.foundClients !== '') {
        return (
            <ul>
                {
                    props.foundClients.map((client, index) => {
                        return <Client key={index} client={client}/>
                    })
                }
            </ul>
        )  
    } else {
        return (
            <ul>
                {
                    props.clients.map((client, index) => {
                        return <Client key={index} client={client}/>
                    })
                }
            </ul>
        )
    }
}
export default connect(
    state => ({
        clients: state.clients,
        foundClients: state.foundClients
    }),
)(ClientList)